#!/bin/bash
#

source ./functions.sh

local cwd=$(pwd)
dotfolders=('bspwm' 'kitty' 'polybar' 'sxhkd')

create_if_not_exist ~/repos

create_if_not_exist ~/.config

cd ~/repos

git clone https://gitlab.com/m3designs/dotfiles.git

cd dotfiles

git checkout origin/fedora-bspwm

for fldr in "${dotfolders[@]}"
do
	ln -s ~/repos/dotfiles/$fldr ~/.config
done

cd $cwd

