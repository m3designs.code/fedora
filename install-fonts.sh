#!/bin/bash

source ./functions.sh

local cwd = $(pwd)

create_if_not_exist ~/repos


git clone https://github.com/ronniedroid/getnf.git

cd getnf
./install.sh
