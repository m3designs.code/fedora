#!/bin/bash

git config --global user.email "mike@mjramello.com"
git config --global user.name "Mike Ramello"

#TODO: Change sudoers file so sudo isn't necessary

sudo sed -i 's/^# %wheel  ALL=(ALL) NOPASSWD: ALL/%wheel  ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

echo "max_parallel_downloads=16" >> /etc/dnf/dnf.conf
echo "fastestmirror=True" >> /etc/dnf/dnf.conf
echo "defaultyes=True" >> /etc/dnf/dnf.conf

echo "UPDATING SYSTEM...."
dnf update -y

echo "INSTALLING PACKAGES...."
dnf install which sddm bspwm sxhkd polybar rofi kitty picom neovim firefox unzip clang fzf ripgrep gem npm -y


echo "SETTING GRAPHICAL TARGET...."
systemctl set-default graphical.target

echo "COPYING CONFIG FILES...."
./link-dotfiles.sh

sed -i 's/^%wheel  ALL=(ALL) NOPASSWD: ALL/# %wheel  ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

echo "INSTALLATION COMPLETE. REBOOT SYSTEM"
